# CollaborNation

![Collabornation Banner](public/TarrigonBanner.png)

This was a website I designed around 2008 for hosting a collaborative writing community. It never went through but here are the remnants.

Most of the focus was going to be on the forum but it never really took off. It's an interesting piece of history though (For me at least).