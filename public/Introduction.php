<font face="Old English Text MT" size="54" color="#000000">S</font>ometime in your future, or maybe in your past, a change of gigantic proportions ripped the heart out of the universe. For eons, many different civilisations of varying degrees of intelligence had co-existed in a galaxy of closely packed planets. Countless points in each of the nation�s history had already been played out, and some had been oblivious to the fact that there was indeed life beyond the stars. Generation after generation had came and gone, and life for each planet went on at the same pace as it had always done for them.<br><br>

That was until the sky exploded with light and then the stars seemed to have the very essence sucked out of them. The less intellectual of the planets thought that this was surely Armageddon. The wisest, helped by their scholars and astronomists had already deduced that a black hole was starting to feast upon the galaxy they lived in.<br><br>

A mad rush then got underway � nations communicating frantically with each other to discuss their impending fate. It was always common law that no nation would interfere with the development of a lesser planet. But how could they sit by and watch millions of people die?<br><br>

In the end a compromise was reached. No nation had the ability to save every single being, so a rescue operation started. Diplomats arrived at every single planet and told their leaders to submit certain gifted individuals on their world to be saved. Some nations refused to believe that their civilisation was about to end, but most did what they could so that their culture and society survived. <br><br>

Meanwhile the galaxies brightest brains came up with a rescue plan. They would all converge on one of the outer most planets � Herakia � whereby it was hoped that the pull of the black hole didn�t extend that far. The Herakian civilisation had long since died out, and the sole survivor had been living on a distant planet. It was agreed that this Herakian, a diarist and mathematician called Tarrigon, would become the new civilisations leader. Albeit grudgingly at first, Tarrigon realised the roles importance, and when the last settlers had landed on Herakia, he had turned and faced these strangers of whom it was his aim to keep alive.<br><br>

He called this beginning �The Living��<br><br>


<font face="Old English Text MT" size="54" color="#003300">D</font><font color="#003300">iary Entry No.1</font>

<p align="right">Date: Unknown. The Living starts now</p>

<i>The stench of fear and foreboding that poured off the skin of the new arrivals was only very slightly tinged with an undercoat of hope. The things that they knew; the people and the places, were soon to be obliterated from existence. It was clear that all they had left were memories and it was this that they had to live on in the next few torrid months. Never had a group of beings; mismatched in all senses of the word, converged in a place with the sole aim of surviving. Why they had designated me leader I do not know. It had to have something to do with me being the only one of them to live on this planet before. However that was a long time ago, and the landscape looked as alien to me as it did to them. They had all looked at me intently, huddled together and fearful. Grown men and women, with their children and many other species, some that even in my many years had not clapped eyes on before. Some I had found to be far too strange to distinguish who or what they were. Above me the skies had turned darker and darker, perhaps signifying our predicament. Here we were in the ruins of a civilisation long dead, but one that I used to be a proud part of. And now we had to make a new civilisation, grown from all the technology, intelligence and ability of every single person that had gathered here. <br><br>

They had expected me to state some rousing speech, one that would fill them with confidence for the lean times ahead. I did have one planned, but all I found myself uttering was plain and pointless air. I�m sure I would have said something, but the deafening explosion of another planet being eaten up had stopped me in my tracks. I don�t think I will ever forget the looks on the faces of some of the planets former inhabitants as they realised they had survived but many lives, maybe friends and family, had been lost. Their whole culture had vanished from history in a heartbeat. I heard the message echoing in my head, like the death knell that had came from the eye of the black hole. This was obviously not a time for talk, but a time for action.<br><br>

I had turned my back on them all and simply whispered, �Let�s get to work�.<br><br></i>

<font face="Old English Text MT" size="54" color="#003300">D</font><font color="#003300">iary Entry No.112</font>
<p align="right">Date: 84 days into The Living</p>

<i>As with previous entries I have to admit being surprised just how the others have coped. Even when I knew that they had been hand selected because of them being talented individuals, I am still in awe with some of their abilities. It is clear that every one of them is still embracing their culture and using it for the good of the group. The city is all but completed, as simply built as it is, but I at last feel safe within its walls. My old people had left us with good foundations to work upon, and all the materials we have needed have been found relatively easily close to us. We haven�t ventured far, the weather being as unpredictable and unforgiving as it has been lately. I am secretly glad about this though, for I still remember some of the horrors that were out in the wilderness of this planet. Perhaps like my people, they too have gone, but I do not intend to find out. I am content here, and I have been trying to hammer home this message to everyone, though it seems I may be losing power. There have been rumblings, especially from the wisest of the group, that we will have to move on sooner or later as the city grows. I pray to the heavens that they will forget this idea. I have only just survived and I do not wish to gamble with death for a trivial matter of exploration.  <br><br></i>


<font face="Old English Text MT" size="54" color="#003300">D</font><font color="#003300">iary Entry No.214</font>
<p align="right">Date: 134 days into the Living</p>

<i>I have finally lost power. After days and days of heated discussion, the others have got their way. An expedition is being mounted to explore outside of the city. Tribes have been set up, made up of all manner of creatures, and will go in different directions out into the horizon. What they will find I shudder to think, and I know for a fact that I won�t be taking part. Some of us will remain, guarding this refuge point just in case things go wrong. I intend to fully chronicle what happens, and to try and keep in touch with the tribes. Communication is the key! They cannot think that they can survive without the rest of the group. We have come so far and splitting up now may bring us back to the ruin that we had fought so hard to escape. I have ordered as my last act that each one of them construct a diary of their exploits so we can plot their course and any new places that they find. If we are going to do this, we must do it properly. I will half-heartedly wish them luck as they depart, but I know that this is the beginning of a new era and one that may come back to haunt us.<br><br></i>

So as the people of Herakia embark upon a new journey, will their lives change for the better? Or will Tarragons fears become a reality? Only time will tell�<br><br>

�This is CollaborNation. Their story is in your hands.