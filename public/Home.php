<font face="Old English Text MT" size="54" color="#000000">W</font>elcome to CollaborNation, the first role-playing site purely dedicated to its members talents for writing. Taking on a character in the world of Herakia, members use their creative expertise to create a day-to-day existence for their character, while interacting with other members to fulfil a variety of tasks, quests and adventures – all just using your skill on a keyboard and an inventive mind. <br><br>
 
<font face="Old English Text MT" size="54" color="#003300">I</font><font color="#003300">magination:</font> Lead your chosen character through an array of tasks and quests. Choose where they go, who they see, and what problems they overcome. Characters have total freedom and are only held back by the boundaries of their imagination.<br><br>
 
<font face="Old English Text MT" size="54" color="#003300">C</font><font color="#003300">o-operation:</font> Interact with other members and try to complete activities either with them or against them. Make clans with your friends or go it alone in your quest to succeed in the C-N world.<br><br>
 
<font face="Old English Text MT" size="54" color="#003300">C</font><font color="#003300">ommunication:</font> Talk to other characters as created by other members. Harness talents, pick up clues and receive help from each one. Take part in C-N every day as new and exciting developments happen hour by hour.<br><br>

The more you take part, the stronger your character grows. The more you post in the forum, the more merchandise you can buy to aid you in the quests. By participating we hope that you have a place to show off a talent in fiction and enjoy being part of a huge collaboration of work.<br><br>

This is CollaborNation…Their future is in your hands.
