<br>
<font face='Old English Text MT' size='+5'>A</font>dvanced Members can get a lot more out of the CN experience. Below is a list of perks that you will receive if you join up.
<br><br>
   <font face='Old English Text MT' size='+3'>1.</font> 5000 CN Tokens on joining up   CN Tokens are the lifeblood of Herakia. Without them you will struggle to make any headway. You receive CN Tokens every time you post in the forum, but why not have a major head start? The CN Tokens you receive when you join up will allow you to buy better merchandise that will in turn open up more quests for you to take part in
<br><br>
<font face='Old English Text MT' size='+3'>2.</font> Ability to do some quests that give real money prizes  CN is always proud of its members writing talents. So much so that we reward them with money on a regular basis. If you are an Advanced Member then the imaginative writing you submit in the forum could end up making you some very real money.
<br><br>
<font face='Old English Text MT' size='+3'>3.</font> A showcase area to place their own creative writing work on the site  CN isnt all about the story of the Herakians. We also want to advertise our members other pieces of work. Advanced Members can post their creative writing in the showcase area. This gets them exposure to all the visitors to the site. Who knows what influential person might take a shine to a particular members story?
<br><br>
<font face='Old English Text MT' size='+3'>4.</font> A picture of your character as drawn by Aphelion  Is your character so vivid that you can picture him or her in your mind? Then why not get Aphelion to draw your character? His talent is clear to see in the Major Characters section. Make your character extra special by having something visual to show off.
<br><br>
<font face='Old English Text MT' size='+3'>5.</font> Your own personal weapon, armour or shield made for you  Does your barbarian need a club befitting of his strength? Or maybe your archer needs a bow fit for her skills? Perhaps your sorcerer needs a cloak to cover his magical talents? Whatever piece of merchandise you need, we can create it for you.
<br><br>
<font face='Old English Text MT' size='+3'>6.</font> Own part of forum, whereby you can submit ideas for new quests, characters and storylines  At CN member input is of an utmost importance to us. We listen to their views and ideas. Advanced Members however get a little extra. They have their own part in the forum where they can discuss the inner workings of CN with its creators.
<br><br>
To sign up for the Advanced Membership, all you will need is an active PAYPAL account. You can get a PAYPAL account via this link <a target="_blank" href="http://www.paypal.com">www.paypal.com</a>. It is the easiest and securest way to pay online.
<br><br>
CN offers three separate ways to pay:
<br><br>
£1.50 for ONE Month Advanced Membership<br>
£3.00 for THREE Months Advanced Membership<br>
£5.00 for SIX Months Advanced Membership
<br><br>
<font size="+2"><div align="center">COMING SOON!</div></font><br />